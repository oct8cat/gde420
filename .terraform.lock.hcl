# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/archive" {
  version = "2.3.0"
  hashes = [
    "h1:OmE1tPjiST8iQp6fC0N3Xzur+q2RvgvD7Lz0TpKSRBw=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version = "0.91.0"
  hashes = [
    "h1:4s+FoYf6bd3ERi4p9l+gRmCXE6Q/s4mDJZ6H1ICTy5c=",
  ]
}
