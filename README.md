# gde420

https://t.me/gde420_bot

```sh
# edit folder_id, admin_sa_id, tg_token
cp example.terraform.tfvars terraform.tfvars
vim terraform.tfvars

# create SA key
yc iam key create --service-account-id=$ADMIN_SA_ID -o admin.key.json

# deploy
terraform apply
```