const {
  S3Client,
  PutObjectCommand,
  GetObjectCommand,
} = require("@aws-sdk/client-s3");
const TelegramBot = require("node-telegram-bot-api");
const assert = require("assert");

const s3Client = new S3Client({ endpoint: process.env.S3_ENDPOINT });
const tgBot = new TelegramBot(process.env.TG_TOKEN);
const bucketParams = {
  Bucket: process.env.BUCKET_NAME,
  Key: process.env.CHATS_FILE_NAME,
};

exports.main = async (event) => {
  const body = JSON.parse(event.body);
  if (!body.message) return { statusCode: 200 };
  const chat = body.message.chat;
  const send = (msg) => tgBot.sendMessage(chat.id, msg);

  if (body.message.text === "/start") {
    const chats = await readChats();
    const existingChat = chats.find((c) => c.id === chat.id);
    if (existingChat) return send(MSG_ALREADY_SUBSCRIBED);

    await writeChats([...chats, chat]);
    await send(MSG_SUBSCRIBED);
  } else if (body.message.text === "/stop") {
    const chats = await readChats();
    const existingChat = chats.find((c) => c.id === chat.id);
    if (!existingChat) return send(MSG_NOT_SUBSCRIBED);

    await writeChats(chats.filter((c) => c !== existingChat));
    await send(MSG_UNSUBSCRIBED);
  }
  return { statusCode: 200 };
};

//

MSG_SUBSCRIBED = "Подписался";
MSG_UNSUBSCRIBED = "Отписался";
MSG_ALREADY_SUBSCRIBED = "Уже подписан";
MSG_NOT_SUBSCRIBED = "Не подписан";

//

const readChats = async () => {
  const res = await s3Client.send(new GetObjectCommand(bucketParams));
  const text = await res.Body.transformToString();
  try {
    const json = JSON.parse(text);
    assert.ok(Array.isArray(json));
    return json;
  } catch (err) {
    return [];
  }
};

const writeChats = async (chats) => {
  await s3Client.send(
    new PutObjectCommand({ ...bucketParams, Body: JSON.stringify(chats) })
  );
};
