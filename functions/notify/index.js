const { S3Client, GetObjectCommand } = require("@aws-sdk/client-s3");
const TelegramBot = require("node-telegram-bot-api");
const { DateTime } = require("luxon");

const s3Client = new S3Client({ endpoint: process.env.S3_ENDPOINT });
const tgBot = new TelegramBot(process.env.TG_TOKEN);
const bucketParams = {
  Bucket: process.env.BUCKET_NAME,
  Key: process.env.CHATS_FILE_NAME,
};
const msg = "Где-то 4:20, держу в курсе";

exports.main = async (event) => {
  const now = DateTime.now();
  console.log(now.zoneName);

  await s3Client
    .send(new GetObjectCommand(bucketParams))
    .then((res) => res.Body.transformToString())
    .then((text) => JSON.parse(text))
    .then((json) => [Array.isArray(json) ? json : []])
    .then(([chats]) =>
      Promise.all(chats.map((c) => tgBot.sendMessage(c.id, msg)))
    );
  return { statusCode: 200 };
};
