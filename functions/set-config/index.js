const { S3Client, PutObjectCommand } = require("@aws-sdk/client-s3");

exports.main = async () => {
  const s3Client = new S3Client({ endpoint: process.env.S3_ENDPOINT });

  await s3Client.send(
    new PutObjectCommand({
      Bucket: process.env.BUCKET_NAME,
      Key: process.env.CHATS_FILE_NAME,
      Body: JSON.stringify([]),
    })
  );

  return { statusCode: 200 };
};
