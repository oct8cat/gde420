const TelegramBot = require("node-telegram-bot-api");

exports.main = async (event) => {
  const bot = new TelegramBot(process.env.TG_TOKEN);
  await bot.setWebHook(event.url);

  return {
    statusCode: 200,
    body: await bot.getWebHookInfo(),
  };
};
