terraform {
  required_providers {
    yandex = { source = "yandex-cloud/yandex" }
  }
}

provider "yandex" {
  folder_id                = var.folder_id
  service_account_key_file = var.admin_sa_key_file
}

#

resource "yandex_iam_service_account_static_access_key" "access_key" {
  service_account_id = var.admin_sa_id
}

resource "yandex_storage_bucket" "bucket" {
  access_key = yandex_iam_service_account_static_access_key.access_key.access_key
  secret_key = yandex_iam_service_account_static_access_key.access_key.secret_key
  bucket     = var.bucket_name
}

# 

locals {
  functions = toset([
    "handle-webhook",
    "notify",
    "set-config",
    "set-webhook",
  ])
  environment = {
    AWS_ACCESS_KEY_ID     = yandex_iam_service_account_static_access_key.access_key.access_key
    AWS_SECRET_ACCESS_KEY = yandex_iam_service_account_static_access_key.access_key.secret_key
    AWS_REGION            = var.region
    S3_ENDPOINT           = var.s3_endpoint
    CHATS_FILE_NAME       = var.chats_file_name
    BUCKET_NAME           = var.bucket_name
    TG_TOKEN              = var.tg_token
  }
}

resource "yandex_function" "function" {
  for_each  = local.functions
  name      = each.key
  user_hash = data.archive_file.function_zip[each.key].output_base64sha256
  content {
    zip_filename = data.archive_file.function_zip[each.key].output_path
  }

  service_account_id = var.admin_sa_id
  runtime            = "nodejs16"
  entrypoint         = "index.main"
  execution_timeout  = 20
  memory             = 128
  environment        = local.environment
}

data "archive_file" "function_zip" {
  for_each    = local.functions
  type        = "zip"
  source_dir  = "functions/${each.key}"
  output_path = ".build/functions/${each.key}.zip"
  excludes    = ["node_modules"]
}

resource "yandex_function_trigger" "trigger" {
  name = var.trigger_name
  timer {
    cron_expression = "20 * * * ? *"
  }
  function {
    id                 = yandex_function.function["notify"].id
    service_account_id = var.admin_sa_id
  }
}

resource "yandex_function_iam_binding" "iam_binding" {
  function_id = yandex_function.function["handle-webhook"].id
  role        = "serverless.functions.invoker"
  members = [
    "system:allUsers",
  ]
}
