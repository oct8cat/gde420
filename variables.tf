variable "folder_id" {
  description = "YC folder id"
}
variable "admin_sa_id" {
  description = "Admin service account id"
}
variable "tg_token" {
  description = "TG Token"
}
variable "region" {
  description = "YC region"
  default     = "ru-central1-a"
}
variable "s3_endpoint" {
  description = "S3 endpoint"
  default     = "https://storage.yandexcloud.net/"
}
variable "admin_sa_key_file" {
  description = "Admin service account key file path"
  default     = "admin.key.json"
}
variable "bucket_name" {
  description = "Storage bucket name"
  default     = "gde420"
}
variable "chats_file_name" {
  description = "Chats file name"
  default     = "chats.json"
}
variable "trigger_name" {
  description = "YC Trigger name"
  default     = "gde420"
}
